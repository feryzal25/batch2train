function birthday(s, d, m) {
  let sum = 0,
    count = 0;
  for (let i = 0; i <= s.length - m; ++i) {
    sum += s[i];
    for (let j = i + m; j-- > i + 1; ) {
      sum += s[j];
    }
    if (sum === d) {
      ++count;
    }
    sum = 0;
  }
  return count;
}

console.log(birthday([2, 2, 1, 3, 2], 4, 2)); // Output: 2
console.log(birthday([1, 2, 1, 3, 2], 3, 2)); // Output: 2
console.log(birthday([1, 1, 1, 1, 1], 3, 2)); // Output: 0
console.log(birthday([4], 4, 1)); // Output: 1
