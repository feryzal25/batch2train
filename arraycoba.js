const Arrr = [1, 2, 3, 4, 5, 6, 7];
const myResult = multiplyArray(Arrr, 2);

function multiplyArray(a, n) {
  const result = [];
  for (let i = n - 1; i < a.length; i += n) {
    result.push(a[i]);
  }
  return result;
}

console.log(myResult); // Output: [2, 4, 6, 8, 10]
