import React from 'react'
import Router from './src/Route2'
import storeRedux from './src/redux/Store'
import { Provider } from 'react-redux'

const App = () => {
  return (
   <Provider store={storeRedux}>
      <Router/>
   </Provider>
  )
}

export default App