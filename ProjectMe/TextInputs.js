import {View, Text, Platform, TextInput, StyleSheet} from 'react-native';
import React, {useState} from 'react';

const textInput = ({placeholders, onChangeTexts, values, heights}) => {
  const [lokalText, setLokalText] = useState('');
  return (
    <View>
      <View
        style={{
          height: 50,
          borderWidth: 3,
          borderRadius: 15,
          marginHorizontal: 20,
          paddingHorizontal: 20,
          marginVertical: 20,
          marginVertical: Platform.OS == 'ios' ? 20 : 5,
          justifyContent: 'center',
          borderColor: 'white',
        }}>
        <TextInput
          placeholder={placeholders}
          placeholderTextColor={'white'}
          value={values}
          onChangeText={lokalText => {
            setLokalText(lokalText);
            onChangeTexts(lokalText);
          }}
          style={{fontWeight: 'bold', color: 'white'}}
        />
      </View>
    </View>
  );
};

export default textInput;
