import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import React, {Component} from 'react';
import Historyy from '../src/image/gambar/Historyy.png';

export class HistoryC extends Component {
  constructor(props) {
    super(props),
      (this.state = {
        nyimpan: [],
      });
  }

  async getData() {
    try {
      var raw = '';

      var requestOptions = {
        method: 'GET',
        body: raw,
        redirect: 'follow',
      };

      fetch(
        'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
        requestOptions,
      )
        .then(response => response.text())
        .then(response => {
          const data = Object.entries(JSON.parse(response));
          this.setState({nyimpan: data});
          // setResult(Object.entries(value));
          console.log(this.state.nyimpan, 'ini datanya');
        })
        .catch(error => console.log('error', error));
    } catch (e) {
      console.log('ini error catch', e);
    }
  }

  componentDidMount() {
    this.getData();
  }

  componentWillUnmount() {
    this.getData();
  }

  render() {
    const arr = this.state.nyimpan;
    return (
      <View
        style={{
          flex: 1,
        }}>
        <ScrollView>
          <View>
            {arr.map((e, index) => {
              return (
                <View style={styles.list} key={index}>
                  <View>
                    <Image
                      source={Historyy}
                      style={{
                        height: 50,
                        width: 50,
                        alignItems: 'center',
                        marginTop: 10,
                      }}
                    />
                  </View>
                  <View>
                    <Text style={styles.data}>ID : {e[0]}</Text>
                    <Text style={styles.data}>Tujuan : {e[1].target}</Text>
                    <Text style={styles.data}>
                      Tipe Transaksi : {e[1].type}
                    </Text>
                    <Text style={styles.data}>Biaya : {e[1].amount}</Text>
                  </View>
                </View>
              );
            })}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  list: {
    marginVertical: 0,
    flexDirection: 'row',
    backgroundColor: '#00b300',
    padding: 10,
    borderRadius: 15,
    borderWidth: 2,
    borderColor: 'white',
    width: 400,
    justifyContent: 'flex-start',
    alignContent: 'center',
  },
  data: {
    fontWeight: 500,
    color: 'white',
    justifyContent: 'center',
    fontFamily: 'Montserrat-Regular',
    marginLeft: 20,
  },
});

export default HistoryC;
