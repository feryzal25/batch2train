import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import React, {useEffect} from 'react';
import {useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

const Homes = () => {
  const [email, setemail] = useState('');
  const [password, setpassword] = useState('');

  const getData = () => {
    const dataToken = async value => {
      await AsyncStorage.setItem('Token', value);
    };
  };

  var data = JSON.stringify({
    name: 'ferizal',
    phone: '0888888888',
    address: 'sumenep',
    password: 'apaaja',
  });

  var config = {
    method: 'get',
    url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
    headers: {
      'Content-Type': 'application/json',
    },
    data: data,
  };

  axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      const data = response.data;
      dataToken(JSON.stringify(data));
      NavigationContainer.navigate('uigojek');
    })
    .catch(function (error) {
      console.log(token, 'data token');
    });
  useEffect(() => {
    getData();
  }, []);

  return (
    <View style={{flex: 1}}>
      <View>
        <TextInput
          placeholder="email"
          placeholderTextColor={'black'}
          value={email}
          onChangeText={e => {
            setemail(e);
          }}></TextInput>

        <TextInput
          placeholder="password"
          placeholderTextColor={'black'}
          value={password}
          onChangeText={e => {
            setpassword(e);
          }}></TextInput>
      </View>
      <View>
        <TouchableOpacity
          onPress={() => {
            getData();
          }}>
          <Text>Get Data</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Homes;
