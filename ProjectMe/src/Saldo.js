import Logo from '../src/image/gambar/logoku.jpg';
import React, {useState} from 'react';
import {StyleSheet, Text, Image, View} from 'react-native';

const App = () => {
  const [balance, setBalance] = useState(1000000);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Image style={styles.logo} source={Logo} />
        <View>
          <Text style={styles.title}>Moeslim Bank</Text>
          <Text
            style={{fontSize: 10, fontWeight: 'bold', alignSelf: 'stretch'}}>
            {'  '}
            Siap Bertransaksi Tanpa Riba!
          </Text>
        </View>
      </View>
      <View
        style={{
          marginTop: 60,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={styles.title2}>Saldo Anda:</Text>
        <View style={styles.balanceContainer}>
          <Text style={styles.balance}>Rp {balance.toLocaleString()}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#98f5f2',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#1fccc6',
    margin: 3,
    borderRadius: 10,
  },
  logo: {
    marginLeft: 10,
    width: 35,
    height: 35,
  },
  title: {
    color: '#0b7874',
    fontSize: 15,
    fontWeight: 'bold',
    marginLeft: 5,
  },
  title2: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  balanceContainer: {
    backgroundColor: '#1fccc6',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    padding: 20,
  },
  balance: {
    fontSize: 36,
    fontWeight: 'bold',
    color: '#fff',
  },
});

export default App;
