import {
  View,
  Text,
  StyleSheet,
  Image,
  BackHandler,
  TextInput,
  Button,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Headerss from './Headerss';

const Transaksi = ({title, navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  const [Rupiahs, setRupiahs] = useState([]);

  const rupiah = x => {
    return x
      ?.toString()
      .replace(/\./g, '')
      .replace(/(\d)(?=(\d{3})+$)/g, '$1' + '.');
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);
  return (
    <View style={styles.container}>
      <View>
        <Headerss title={'Transaksi'} navigation={navigation} />
        <View>
          <Transaksi title={'Transaksi'} />
          <TextInput
            placeholder="Masukkan Nominal"
            placeholderTextColor={'black'}
            keyboardType={'number-pad'}
            value={rupiah(Rupiahs)}
            onChangeText={e => {
              const ilang = e.replace(/\./g, ' ');
              console.log(ilang, 'ini apa si');
              if (ilang.length >= 12) {
                return true;
              } else {
                setRupiahs(e);
              }
            }}
          />
          <Button title="Submit" onPress={handleButtonPress} />
        </View>
      </View>
      <View style={styles.back}>
        <View>
          <Text style={styles.text}>{title}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    padding: 10,
    backgroundColor: 'white',
    elevation: 5,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'column',
  },
  back: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: 5,
  },
  text: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'black',
  },
});

export default Transaksi;
