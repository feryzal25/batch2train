import {View, Text, TextInput, StyleSheet} from 'react-native';
import React, {useState} from 'react';

const TextInputLabel = ({
  placeholder,
  Label,
  values,
  onChangeTexts,
  TypeKey,
}) => {
  const [localValue, setLocalValue] = useState('');
  return (
    <View>
      <View>
        <Text style={{color: 'black'}}>{Label}</Text>
      </View>
      <View style={style.TexInputCon}>
        <TextInput
          placeholder={placeholder}
          value={values}
          onChangeText={text => {
            setLocalValue(text);
            onChangeTexts(text);
          }}
          keyboardType={TypeKey}
        />
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  TexInputCon: {
    marginTop: 5,
    borderWidth: 1,
    borderRadius: 100,
    borderColor: '#1fccc6',
  },
});

export default TextInputLabel;
