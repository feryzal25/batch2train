import {createStore, applyMiddleware} from 'redux';
import rootReducer from './Reducer/Index';
import thunk from 'redux-thunk';

//create store untuk save a data sebagai penengah
const storeRedux = createStore(rootReducer, applyMiddleware(thunk));

export default storeRedux;
