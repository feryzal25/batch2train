import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch} from 'react-redux';

export const getData = () => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };
  return async dispatch => {
    var data = JSON.stringify({
      username: 'test',
      mail: 'test@mail.com',
      password: '12345678',
    });

    var config = {
      method: 'get',
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        const data = response.data;
        dispatch({
          type: 'LOGIN',
          data: data,
        });
        dataToken(JSON.stringify(data));
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const Masuk = (email, password) => {
  return async dispatch => {
    try {
      const response = await axios.post(
        'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users.json',
        {
          email: email,
          password: password,
        },
      );
      const data = response.data;
      dispatch({
        type: 'LOGIN',
        data: data,
      });
      await AsyncStorage.setItem('Token', data.token);
    } catch (error) {
      console.log(error);
    }
  };
};

export const SignUp = (username, email, password) => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };
  return dispatch => {
    var data = JSON.stringify({
      username: username,
      mail: email,
      password: password,
    });

    var config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users.json',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        dataToken(JSON.stringify(response.data));
        const data = JSON.stringify(response.data);
        dispatch({
          type: 'REGISTER',
          data: data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const History = () => {
  return async dispatch => {
    var data = '';

    var config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {},
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        const data = response.data;
        dispatch({
          type: 'GET_HISTORY',
          data: data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const PayTransaction = (
  dataAmount,
  dataSender,
  dataTarget,
  dataType,
) => {
  return async dispatch => {
    var data = JSON.stringify({
      amount: dataAmount,
      sender: dataSender,
      target: dataTarget,
      type: dataType,
    });

    var config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data), 'Berhasil Hore');
        const data = JSON.stringify(response.data);
        dispatch({
          type: 'TRANSAKSI',
          data: data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const Deletes = () => {
  const data = '';
  return async dispatch => {
    dispatch({
      type: 'SIGN_OUT',
      data: data,
    });
  };
};
