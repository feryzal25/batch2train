const initialState = {
  AuthData: null,
};

const Auth = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        AuthData: action.data,
      };
    case 'REGISTER':
      return {
        ...state,
        AuthData: action.data,
      };
    case 'SIGN_OUT':
      return {
        ...state,
        AuthData: action.data,
      };
    default:
      return state;
  }
};

export default Auth;
