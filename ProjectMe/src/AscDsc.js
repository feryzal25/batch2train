import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Button,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';

const AscDsc = ({onPress1, onPress2}) => {
  const [data, setData] = useState([]);

  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-end',
        }}>
        <TouchableOpacity onPress={onPress1}>
          <View
            style={{
              width: '5%',
              alignItems: 'flex-start',
              justifyContent: 'flex-start',
            }}>
            <Image
              source={{
                uri: 'https://img.icons8.com/windows/32/000000/sort-numeric-up.png',
              }}
              style={{
                height: 23,
                width: 40,
                resizeMode: 'contain',
                marginTop: 5,
                marginRight: -6,
              }}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={onPress2}>
          <View
            style={{
              width: '5%',
              alignItems: 'flex-start',
              justifyContent: 'flex-start',
            }}>
            <Image
              source={{
                uri: 'https://img.icons8.com/ios-glyphs/30/000000/numerical-sorting-21.png',
              }}
              style={{
                height: 23,
                width: 40,
                resizeMode: 'contain',
                marginTop: 5,
              }}
            />
          </View>
        </TouchableOpacity>
      </View>
      <View></View>
    </View>
  );
};

const styles = StyleSheet.create({
  list: {
    justifyContent: 'center',
    alignContent: 'center',
    padding: 8,
    elevation: 1.5,
    borderRadius: 5,
    backgroundColor: 'white',
    marginTop: 8,
  },
  data: {
    color: 'black',
    fontFamily: 'Montserrat-Regular',
    marginLeft: 10,
  },
});

export default AscDsc;
