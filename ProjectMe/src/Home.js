import Logo from '../src/image/gambar/logoku.jpg';
import Saldo from '../src/image/gambar/Saldo.gif';
import Transfer from '../src/image/gambar/Transaksi.gif';
import Loghis from '../src/image/gambar/history.gif';
import About from '../src/image/gambar/information.gif';
import Logomid from '../src/image/gambar/logomid.png';
import React from 'react';
import {useDispatch} from 'react-redux';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Deletes} from './redux/Action/Action';
import History from './History';

const App = ({navigation}) => {
  const Log = async () => {
    try {
      dispatch(Deletes());
      await AsyncStorage.removeItem('Token');
    } catch (e) {
      console.log(e, 'error');
    }
  };

  const dispatch = useDispatch();
  async function logout() {
    try {
      console.log('Keluar');
      dispatch(Deletes());
      await AsyncStorage.removeItem('Token');
    } catch (e) {
      console.log('error', e);
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Image style={styles.logo} source={Logo} />
        <View>
          <Text style={styles.title}>Moeslim Bank</Text>
          <Text
            style={{fontSize: 10, fontWeight: 'bold', alignSelf: 'stretch'}}>
            {'  '}
            Siap Bertransaksi Tanpa Riba!
          </Text>
        </View>
      </View>
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
        <Text style={styles.bodyTitle}>HI, MARHABAN!</Text>
        <Image style={styles.logoMid} source={Logomid} />
      </View>
      <View style={styles.menu}>
        <View>
          <View style={styles.inputContainer}>
            <View>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Saldo');
                }}
                style={styles.menuButton}>
                <Image style={styles.menuIcon} source={Saldo} />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Bayar');
              }}
              style={styles.menuButton}>
              <Image style={styles.menuIcon} source={Transfer} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Historys');
              }}
              style={styles.menuButton}>
              <Image style={styles.menuIcon} source={Loghis} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('About');
              }}
              style={styles.menuButton}>
              <Image style={styles.menuIcon} source={About} />
            </TouchableOpacity>
          </View>
        </View>
        <View>
          <View style={styles.textkolom}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Saldo');
              }}>
              <Text style={styles.menuText}>SALDO</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.textkolom}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Bayar');
              }}>
              <Text style={styles.menuText}>PEMBAYARAN</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.textkolom}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Historys');
              }}>
              <Text style={styles.menuText}>LIST TRANSAKSI</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.textkolom}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('About');
              }}>
              <Text style={styles.menuText}>TENTANG</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
        }}>
        <Text style={{fontSize: 16, fontWeight: 'bold'}}>
          JAZAKUMULLAHU KHAIRAN!!
        </Text>
        <Text style={{fontSize: 12, fontWeight: '500'}}>
          Semoga Stay Halal No Riba Selamanya Sampai Surga
        </Text>
      </View>
      <View style={styles.body}>
        <TouchableOpacity
          style={styles.button}
          onPress={async () => {
            logout();
          }}>
          <Text style={styles.buttonText}>LOGOUT</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#98f5f2',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#1fccc6',
    margin: 3,
    borderRadius: 10,
  },
  logo: {
    marginLeft: 10,
    width: 35,
    height: 35,
  },
  title: {
    color: '#0b7874',
    fontSize: 15,
    fontWeight: 'bold',
    marginLeft: 5,
  },
  logoMid: {
    height: 320,
    width: 320,
    marginTop: -110,
  },
  menu: {
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  menuButton: {
    alignItems: 'center',
  },
  menuIcon: {
    width: 50,
    height: 50,
  },
  menuText: {
    marginTop: 7,
    marginBottom: 7,
    fontWeight: 'bold',
  },
  textkolom: {
    backgroundColor: '#fff',
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center',
    width: 150,
    borderRadius: 25,
  },
  inputContainer: {
    backgroundColor: '#fff',
    padding: 10,
    marginLeft: 30,
    borderRadius: 25,
    marginBottom: 10,
    justifyContent: 'center',
    alignContent: 'center',
    width: 80,
  },
  body: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bodyTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 20,
  },
  bodyText: {
    fontSize: 16,
    textAlign: 'center',
    marginHorizontal: 20,
    marginBottom: 40,
  },
  button: {
    backgroundColor: '#1fccc6',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
  },
  buttonText: {
    color: '#076b67',
    fontWeight: 'bold',
    fontSize: 16,
  },
});

export default App;
