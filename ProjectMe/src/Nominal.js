import {View, Text} from 'react-native';
import React, {useState} from 'react';
import {ScrollView} from 'react-native';
import {TextInput} from 'react-native';

const Nominal = value => {
  const [Rupiahs, setRupiahs] = useState(0);
  const rupiah = x => {
    return x
      ?.toString()
      .replace(/\./g, '')
      .replace(/(\d)(?=(\d{3})+$)/g, '$1' + '.');
  };

  return (
    <View>
      <ScrollView showsVerticalScrollIndicator={true}>
        <View>
          <TextInput
            placeholder="Masukkan Nominal"
            placeholderTextColor={'black'}
            keyboardType={'number-pad'}
            value={Rupiahs}
            onChangeText={e => {
              const ilang = e.replace(/\./g, ' ');
              console.log(ilang, 'ini apa si');
              if (ilang.length >= 12) {
                return true;
              } else {
                setRupiahs(rupiah(e));
              }
            }}></TextInput>
        </View>
      </ScrollView>
    </View>
  );
};

export default Nominal;
