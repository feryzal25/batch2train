import LoginScreen from './Login';
import RegisterScreen from './Register';
import Home from './Home';
import Historys from './History2';
import Bayar from './Bayar';
import Saldo from './Saldo';
import About from './About';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useSelector} from 'react-redux';

const Stack = createNativeStackNavigator();

//khusus home, semua navigasi tidak bersangkutan dengan login dan resister
// dan disini juga logout
const AppStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Historys" component={Historys} />
      <Stack.Screen name="Bayar" component={Bayar} />
      <Stack.Screen name="Saldo" component={Saldo} />
      <Stack.Screen name="About" component={About} />
    </Stack.Navigator>
  );
};

//khusus untuk login, register, splashscreen
const AuthStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="LoginScreen" component={LoginScreen} />
      <Stack.Screen name="Register" component={RegisterScreen} />
    </Stack.Navigator>
  );
};

function Router() {
  const [tokens, setTokens] = useState();
  const UpdateData = useSelector(state => state.Auth.AuthData);
  const DataUser = async () => {
    const tokens = await AsyncStorage.getItem('Token');
    setTokens(JSON.parse(tokens));
  };
  useEffect(() => {
    DataUser();
  }, [UpdateData]);

  return (
    <NavigationContainer>
      {tokens ? <AppStack /> : <AuthStack />}
    </NavigationContainer>
  );
}
export default Router;
