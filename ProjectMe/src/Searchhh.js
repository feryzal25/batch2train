import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Image,
  ScrollView,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Headerss from './Headerss';

const Search = navigation => {
  const [text, settext] = useState('');

  useEffect(() => {
    getData();
  }, []);

  const [result, setResult] = useState([]);
  // const [databaru, setDatabaru] = useState();
  const getData = async () => {
    var raw = '';

    var requestOptions = {
      method: 'GET',
      body: raw,
      redirect: 'follow',
    };

    fetch(
      'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      requestOptions,
    )
      .then(response => response.text())
      .then(async response => {
        const value = JSON.parse(response);
        setResult(Object.entries(value));
      })
      .catch(error => console.log('error', error));
  };

  return (
    <View style={styles.container}>
      <Headerss title={'History'} navigation={navigation} />
      <View style={styles.top1}>
        <Image
          source={{
            uri: 'https://img.icons8.com/ios-filled/512/search--v1.png',
          }}
          style={{marginLeft: 10, height: 23, width: 40, resizeMode: 'contain'}}
        />
        <TextInput
          style={{
            height: 40,
            borderColor: 'gray',
            width: '90%',
          }}
          onChangeText={text => settext(text)}
          value={text}
          placeholder="Masukkan tipe yang kamu cari!"
        />
      </View>

      <ScrollView>
        {result?.map((e, index) => {
          const filter = e.filter(item => {
            return (
              item?.type?.toLowerCase().includes(text?.toLowerCase()) ||
              item?.amount
                ?.toString()
                .toLowerCase()
                .includes(text?.toLowerCase()) ||
              item?.sender
                ?.toString()
                .toLowerCase()
                .includes(text?.toLowerCase()) ||
              item?.target
                ?.toString()
                .toLowerCase()
                .includes(text?.toLowerCase())
            );
          });
          return (
            <View>
              {filter.map((es, index) => {
                return (
                  <View style={styles.list} key={index}>
                    <Text style={styles.data}>Sender : {es.sender}</Text>
                    <Text style={styles.data}>Target : {es.target}</Text>
                    <Text style={styles.data}>Jumlah : {es.amount}</Text>
                    <Text style={styles.data}>Tipe Transaksi : {es.type}</Text>
                  </View>
                );
              })}
            </View>
          );
        })}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  top1: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 20,
    borderColor: '#d9d9d9',
    height: 45,
    width: '90%',
    marginTop: 10,
    marginBottom: 10,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  list: {
    justifyContent: 'center',
    alignContent: 'center',
    padding: 8,
    elevation: 1.5,
    borderRadius: 5,
    backgroundColor: 'white',
    marginTop: 8,
  },
  data: {
    color: 'black',
    fontFamily: 'Montserrat-Regular',
    marginLeft: 10,
  },
});

export default Search;
