import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import React from 'react';
import Mata from '../src/image/eyeclose.png';
import Mata2 from '../src/image/EyeOp.png';

const TextInputCos = ({
  placeholders,
  onChangeTexts,
  values,
  heights,
  Secure,
  Ispas,
  onPresss,
}) => {
  const [lokalText, setlokalText] = useState('');
  return (
    <View>
      <View
        style={{
          height: heights ? heights : 50,
          borderWidth: 1,
          borderRadius: 8,
          marginHorizontal: 10,
          flexDirection: 'row',
          paddingHorizontal: 5,
          alignItems: 'center',
        }}>
        <TextInput
          placeholder={placeholders}
          placeholderTextColor={'black'}
          value={values}
          onChangeText={Text => {
            setlokalText(Text);
            onChangeTexts(Text);
          }}
          secureTextEntry={Secure}
          style={{width: '90%'}}
        />
        {Ispas ? (
          <>
            <TouchableOpacity
              style={{
                alignSelf: 'center',
                justifyContent: 'center',
                height: '90%',
                width: '10%',
              }}
              onPress={() => {
                onPresss();
              }}>
              <image
                source={Secure == true ? Mata : Mata2}
                style={{resizeMode: 'contain', height: 30}}
              />
            </TouchableOpacity>
          </>
        ) : (
          <></>
        )}
      </View>
    </View>
  );
};

export default TextInputCos;
