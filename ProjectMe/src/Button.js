import React, {useState, useEffect} from 'react';
import {View, Text, Button, StyleSheet, ScrollView} from 'react-native';

const Asc = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    // Ambil data dari API menggunakan metode GET
    fetch(
      'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
    )
      .then(response => response.json())
      .then(data => {
        const dataArray = Object.keys(data).map(key => ({
          sender: data[key].sender,
          target: data[key].target,
          amount: data[key].amount,
          type: data[key].type,
        }));
        setData(dataArray);
      })
      .catch(error => {
        console.error('Error:', error);
        setData([]);
      });
  }, []);

  const sortAscending = () => {
    const sortedData = [...data].sort((a, b) => a.amount - b.amount);
    setData(sortedData);
  };

  const sortDescending = () => {
    const sortedData = [...data].sort((a, b) => b.amount - a.amount);
    setData(sortedData);
  };

  if (data.length === 0) {
    return <Text>Loading...</Text>;
  }

  return (
    <View>
      <Button title="Sort Ascending" onPress={sortAscending} />
      <Button title="Sort Descending" onPress={sortDescending} />
      <View>
        <ScrollView showsVerticalScrollIndicator={false}>
          {data.length > 0 ? (
            data.map((es, index) => (
              <View style={styles.list} key={index}>
                <Text style={styles.data}>Sender : {es.sender}</Text>
                <Text style={styles.data}>Target : {es.target}</Text>
                <Text style={styles.data}>Jumlah : {es.amount}</Text>
                <Text style={styles.data}>Tipe Transaksi : {es.type}</Text>
              </View>
            ))
          ) : (
            <Text>No data</Text>
          )}
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  list: {
    justifyContent: 'center',
    alignContent: 'center',
    padding: 8,
    elevation: 1.5,
    borderRadius: 5,
    backgroundColor: 'white',
    marginTop: 8,
  },
  data: {
    color: 'black',
    fontFamily: 'Montserrat-Regular',
    marginLeft: 10,
  },
});

export default Asc;
