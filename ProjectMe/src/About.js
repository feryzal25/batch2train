import Logo from '../src/image/gambar/logoku.jpg';
import React from 'react';
import {StyleSheet, Image, Text, View} from 'react-native';

const AboutMe = () => {
  return (
    <View style={styles.container}>
      <Image style={{height: 100, width: 100}} source={Logo} />
      <Text style={styles.title}>Tentang MoesBank</Text>
      <Text style={styles.description}>
        Aplikasi mobile banking kami hadir untuk memudahkan Anda dalam melakukan
        transaksi perbankan kapan saja dan di mana saja. Dengan aplikasi ini,
        Anda dapat mengakses rekening Anda, memeriksa saldo, mengirim dan
        menerima uang, membayar tagihan, dan masih banyak lagi.
      </Text>
      <Text style={styles.description}>
        Kami selalu mengutamakan keamanan dan privasi informasi Anda dengan
        menggunakan teknologi canggih dan standar keamanan yang tinggi.
      </Text>
      <Text style={styles.description}>
        Kami bekerja sama dengan berbagai bank terkemuka di Indonesia untuk
        memberikan layanan perbankan yang berkualitas dan terpercaya. Tim kami
        selalu siap membantu Anda dalam mengatasi masalah dan memberikan saran
        yang tepat terkait penggunaan aplikasi ini.
      </Text>
      <Text style={styles.description}>
        Kami berkomitmen untuk terus meningkatkan kualitas dan fitur aplikasi
        ini agar dapat memenuhi kebutuhan perbankan Anda. Jika Anda memiliki
        saran atau masukan, jangan ragu untuk menghubungi kami melalui fitur
        dukungan dalam aplikasi ini. Terima kasih telah menggunakan aplikasi
        mobile banking kami.
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#98f5f2',
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  description: {
    fontSize: 16,
    marginBottom: 10,
  },
});

export default AboutMe;
