import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import React from 'react';

const Headerss = ({title, navigation}) => {
  return (
    <View style={styles.header}>
      <View style={styles.back}>
        <View>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Uigojek');
            }}>
            <Image
              source={{
                uri: 'https://img.icons8.com/external-royyan-wijaya-detailed-outline-royyan-wijaya/512/external-arrow-arrow-line-royyan-wijaya-detailed-outline-royyan-wijaya-5.png',
              }}
              style={{
                height: 23,
                width: 40,
                resizeMode: 'contain',
              }}
            />
          </TouchableOpacity>
        </View>
        <View>
          <Text style={styles.text}>{title}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    padding: 10,
    backgroundColor: 'white',
    elevation: 5,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'column',
  },
  back: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: 5,
  },
  text: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'black',
  },
});

export default Headerss;
