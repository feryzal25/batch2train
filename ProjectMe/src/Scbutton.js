import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import React, {useState} from 'react';

const ScButton = () => {
  const [Change, setChange] = useState(0);
  const ChangeColor = itemID => {
    setChange(currentItem => (currentItem === itemID ? itemID : itemID));
  };
  const datass = [
    {label: 'Konten Buat Kamu', id: 0},
    {label: 'Apa Aja', id: 1},
    {label: 'Hiburan', id: 2},
    {label: 'Makanan', id: 3},
    {label: 'Gaya Hidup', id: 4},
  ];
  return (
    <View>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        {datass.map(item => {
          const colors = Change == item.id ? '#fff' : '#000';
          return (
            <TouchableOpacity
              onPress={() => {
                ChangeColor(item.id);
              }}>
              <View style={style.conData(Change, item.id)}>
                <Text
                  style={{
                    color: colors,
                  }}>
                  {item.label}
                </Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
};

const style = StyleSheet.create({
  conData: (click, data) => ({
    backgroundColor: click !== data ? '#fff' : '#00B200',
    borderRadius: 24,
    height: 34,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 16,
    paddingVertical: 6,
    marginRight: 8,
    borderWidth: 1,
    borderColor: click !== data ? '#d9d9d9' : '#00B200',
  }),
});

export default ScButton;
