import {
  View,
  Text,
  BackHandler,
  StyleSheet,
  ScrollView,
  Image,
} from 'react-native';
import Headerss from './Headerss';
import React, {useState, useEffect} from 'react';
import Historyy from '../src/image/gambar/Historyy.png';
import Search from './Searchhh';

const History = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  const [result, setResult] = useState([]);
  const getData = async () => {
    var raw = '';

    var requestOptions = {
      method: 'GET',
      body: raw,
      redirect: 'follow',
    };

    fetch(
      'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      requestOptions,
    )
      .then(response => response.text())
      .then(response => {
        const value = JSON.parse(response);
        setResult(Object.entries(value));
      })
      .catch(error => console.log('error', error));
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  useEffect(() => {
    getData();
  }, []);

  return (
    <View style={styles.container}>
      <View>
        <Headerss title={'History'} navigation={navigation} />
        <Search />

        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.body}>
            {result.map((e, index) => {
              return (
                <View style={styles.list} key={index}>
                  <View>
                    <Image
                      source={Historyy}
                      style={{
                        height: 50,
                        width: 50,
                        alignItems: 'center',
                        marginTop: 10,
                      }}
                    />
                  </View>

                  <View>
                    <Text style={styles.data}>ID : {e[0]}</Text>
                    <Text style={styles.data}>Tujuan : {e[1].target}</Text>
                    <Text style={styles.data}>
                      Tipe Transaksi : {e[1].type}
                    </Text>
                    <Text style={styles.data}>Biaya : {e[1].amount}</Text>
                  </View>
                </View>
              );
            })}
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    marginVertical: 0,
    flexDirection: 'row',
    backgroundColor: '#00b300',
    padding: 10,
    borderRadius: 15,
    borderWidth: 2,
    borderColor: 'white',
    width: 400,
    justifyContent: 'flex-start',
    alignContent: 'center',
  },
  body: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  data: {
    fontWeight: 500,
    color: 'white',
    justifyContent: 'center',
    fontFamily: 'Montserrat-Regular',
    marginLeft: 20,
  },
  text: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 25,
    color: 'black',
    marginHorizontal: 25,
    paddingBottom: 10,
  },
});

export default History;
