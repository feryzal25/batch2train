import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  Button,
  Alert,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Putih from '../src/image/gambar/Putih.jpeg';
import Logo from '../src/image/gambar/logoku.jpg';
import {getData} from './redux/Action/Action';

const validateEmail = email => {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return emailRegex.test(email);
};

const validatePassword = password => {
  return password.length >= 8;
};

const LoginScreen = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [loginError, setLoginError] = useState('');

  const validatorEmail = () => {
    if (!validateEmail(email)) {
      setEmailError('Please enter a valid email address');
      return false;
    }
    return true;
  };
  const validatPass = () => {
    if (!validatePassword(password)) {
      setPasswordError('Password must be at least 8 characters long');
      return false;
    }
    return true;
  };
  const handleLogin = () => {
    if (validatorEmail() && validatPass()) {
      Alert.alert('Berhasil Login', 'Lanjut ke Aplikasi?', [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => login()},
      ]);
    }
  };

  const dispatch = useDispatch();
  async function login() {
    try {
      dispatch(getData());
    } catch (e) {
      console.log('errors', e);
    }
  }

  const DataUser = async () => {
    const token = await AsyncStorage.getItem('Token');
    console.log(token, 'data token');
  };
  useEffect(() => {
    DataUser();
  }, []);
  const state = useSelector(state => state.Auth);
  console.log(state, 'ini datanya');

  return (
    <KeyboardAvoidingView
      style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}
      behavior={Platform.OS === 'ios' ? 'padding' : null}>
      <ScrollView>
        <ImageBackground source={Putih} style={styles.backgroundImage}>
          <View style={styles.container}>
            <Image
              source={Logo}
              style={{
                width: 200,
                height: 200,
                marginBottom: 20,
                alignSelf: 'center',
                alignItems: 'baseline',
              }}
            />
            <Text
              style={{
                color: '#068f8f',
                alignSelf: 'center',
                fontSize: 24,
                fontWeight: 'bold',
              }}>
              Welcome to MoesBank!
            </Text>
            <Text
              style={{color: 'black', alignSelf: 'center', marginBottom: 10}}>
              HIDUP TANPA RIBA HINGGA SURGA
            </Text>
            <View style={styles.inputContainer}>
              <Text style={styles.label}>Email</Text>
              <TextInput
                placeholder="Masukkan Email Anda!"
                value={email}
                onChangeText={setEmail}
                onBlur={() => {
                  setEmailError(
                    validateEmail(email)
                      ? ''
                      : 'Please enter a valid email address',
                  );
                }}
                style={styles.input}
              />
              {emailError ? (
                <Text style={styles.error}>{emailError}</Text>
              ) : null}

              <Text style={styles.label}>Password</Text>
              <TextInput
                placeholder="Input Password"
                value={password}
                onChangeText={setPassword}
                onBlur={() =>
                  setPasswordError(
                    validatePassword(password)
                      ? ''
                      : 'Password must be at least 8 characters long',
                  )
                }
                secureTextEntry
                style={styles.input}
              />
              {passwordError ? (
                <Text style={styles.error}>{passwordError}</Text>
              ) : null}
            </View>

            {loginError ? <Text style={styles.error}>{loginError}</Text> : null}

            <View style={styles.button}>
              <TouchableOpacity
                onPress={() => {
                  handleLogin();
                }}>
                <Text
                  style={{
                    color: 'white',
                    alignItems: 'center',
                    justifyContent: 'center',
                    textAlign: 'center',
                  }}>
                  SUBMIT
                </Text>
              </TouchableOpacity>
            </View>

            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: 13,
                }}>
                Anda belum punya akun?
              </Text>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Register');
                }}>
                <Text
                  style={{
                    fontSize: 10,
                    fontWeight: 'bold',
                  }}>
                  Registrasi disini!
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    width: 400,
  },
  backgroundImage: {
    flex: 1,
    color: '#d6e5f2',
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center',
    height: 660,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    color: 'white',
    textAlign: 'center',
  },
  inputContainer: {
    backgroundColor: '#cdcdcd',
    padding: 20,
    marginLeft: 30,
    borderRadius: 10,
    marginBottom: 10,
    justifyContent: 'center',
    alignContent: 'center',
    width: 320,
  },
  label: {
    fontSize: 16,
    marginBottom: 10,
    color: '#333',
  },
  input: {
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 5,
    padding: 10,
  },
  button: {
    backgroundColor: '#068f8f',
    borderRadius: 7,
    marginTop: 5,
    marginEnd: 90,
    marginStart: 90,
    paddingVertical: 10,
    paddingHorizontal: 10,
    margin: 10,
  },
  error: {
    color: 'red',
    marginTop: 10,
  },
});

export default LoginScreen;
