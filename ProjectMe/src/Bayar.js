import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ToastAndroid,
} from 'react-native';
import React, {useState} from 'react';
import TextInputLabel from './TextInputLabel';
import {useDispatch} from 'react-redux';
import {useEffect} from 'react';
import Logo from '../src/image/gambar/logoku.jpg';
import {PayTransaction} from './redux/Action/Action';
import {RadioButton} from 'react-native-paper';

const Bayar = () => {
  const [disables, setDisables] = useState(false);
  const [dataAmount, setDataAmount] = useState('');
  const [dataSender, setDataSender] = useState('');
  const [dataTarget, setDataTarget] = useState('');
  const [dataType, setDataType] = useState('');

  const dispatch = useDispatch();

  const ValidatorButton = () => {
    return dataAmount !== '' &&
      dataSender !== '' &&
      dataTarget !== '' &&
      dataType !== ''
      ? setDisables(false)
      : setDisables(true);
  };
  useEffect(() => {
    ValidatorButton();
  }, [dataAmount, dataSender, dataTarget, dataType]);

  const showToast = () => {
    ToastAndroid.show('Transaksi Berhasil', ToastAndroid.SHORT);
  };

  const rupiah = x => {
    return x
      ?.toString()
      .replace(/\./g, '')
      .replace(/(\d)(?=(\d{3})+$)/g, '$1' + '.');
  };

  async function pay(e) {
    try {
      dispatch(PayTransaction(dataAmount, dataSender, dataTarget, dataType));
      console.log('Transaksi Berhasil');
    } catch (e) {
      console.log('error', e);
    }
  }

  const handlePay = () => {
    pay();
  };

  return (
    <View style={style.container}>
      <View style={style.header}>
        <Image style={style.logo} source={Logo} />
        <View>
          <Text style={style.title}>Moeslim Bank</Text>
          <Text
            style={{fontSize: 10, fontWeight: 'bold', alignSelf: 'stretch'}}>
            {'  '}
            Siap Bertransaksi Tanpa Riba!
          </Text>
        </View>
      </View>
      <View>
        <View>
          <View
            style={{
              marginHorizontal: 15,
            }}>
            <TextInputLabel
              Label={'Nominal'}
              placeholder={'Masukan Nominal'}
              values={dataAmount}
              onChangeTexts={text => {
                setDataAmount(rupiah(text || ''));
              }}
              TypeKey={'number-pad'}
            />
          </View>
          <View
            style={{
              marginTop: 10,
              marginHorizontal: 15,
            }}>
            <TextInputLabel
              Label={'Rekening Asal'}
              placeholder={'Masukan Rekening'}
              values={dataSender}
              onChangeTexts={text => {
                setDataSender(text);
              }}
              TypeKey={'number-pad'}
            />
          </View>
          <View
            style={{
              marginHorizontal: 15,
              marginTop: 10,
            }}>
            <TextInputLabel
              Label={'Rekening Tujuan'}
              placeholder={'Masukan Rekening'}
              values={dataTarget}
              onChangeTexts={text => {
                setDataTarget(text);
              }}
            />
          </View>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
          <View style={{flexDirection: 'row'}}>
            <RadioButton
              value="pulsa"
              status={dataType === 'pulsa' ? 'checked' : 'unchecked'}
              onPress={() => setDataType('pulsa')}
              color="black"></RadioButton>
            <Text style={{marginTop: 7, fontWeight: 'bold'}}> Pulsa</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <RadioButton
              value="transfer"
              status={dataType === 'transfer' ? 'checked' : 'unchecked'}
              onPress={() => setDataType('transfer')}
              color="black"></RadioButton>
            <Text style={{marginTop: 7, fontWeight: 'bold'}}> Transfer</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <RadioButton
              value="Emoney"
              status={dataType === 'Emoney' ? 'checked' : 'unchecked'}
              onPress={() => setDataType('Emoney')}
              color="black"></RadioButton>
            <Text style={{marginTop: 7, fontWeight: 'bold'}}> E-Money</Text>
          </View>
        </View>
        <View>
          <TouchableOpacity
            disabled={disables}
            onPress={() => {
              handlePay();
              showToast();
            }}
            style={{
              backgroundColor: disables == false ? '#1fccc6' : '#f0f3f4',
              marginHorizontal: 20,
              paddingVertical: 15,
              paddingHorizontal: 20,
              marginTop: 8,
              borderRadius: 8,
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: disables == false ? 'white' : '#1fccc6',
                fontSize: 18,
                fontWeight: 'bold',
              }}>
              Kirim
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#98f5f2',
  },
  option: {
    height: 130,
    paddingTop: 20,
    backgroundColor: '#98f5f2',
  },
  confrims: {
    backgroundColor: '#1fccc6',
    alignItems: 'center',
    marginHorizontal: 15,
    paddingVertical: 10,
    marginTop: 20,
    borderRadius: 10,
    elevation: 5,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#1fccc6',
    margin: 3,
    borderRadius: 10,
  },
  logo: {
    marginLeft: 10,
    width: 35,
    height: 35,
  },
  title: {
    color: '#0b7874',
    fontSize: 15,
    fontWeight: 'bold',
    marginLeft: 5,
  },
});

export default Bayar;
