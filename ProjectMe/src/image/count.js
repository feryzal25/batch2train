import {View, Text, TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';

const Count = () => {
  const [count, setcount] = useState(0);

  const increment = () => {
    setcount(count + 1);
  };

  const decrem = () => {
    setcount(count - 1);
  };

  useEffect(() => {
    console.log(count);
  }),
    [count];

  return (
    <View style={{flex: 1, justifyContent: 'center'}}>
      <Text
        style={{
          alignSelf: 'center',
          color: 'black',
          fontSize: 16,
          fontWeight: 'bold',
        }}>
        {count}
      </Text>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => {
            increment();
          }}>
          <Text
            style={{
              color: 'red',
            }}>
            Tambah
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            decrem();
          }}>
          <Text
            style={{
              color: 'red',
            }}>
            Kurang
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Count;
