import {View, Text, BackHandler} from 'react-native';
import React, {useEffect} from 'react';

const GoSend = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  return (
    <View>
      <Text style={{fontWeight: 'bold'}}>GoSend</Text>
    </View>
  );
};

export default GoSend;
