import {View, Text, BackHandler} from 'react-native';
import React, {useEffect} from 'react';

const GoPoints = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  return (
    <View>
      <Text style={{fontWeight: 'bold'}}>GoPoints</Text>
    </View>
  );
};

export default GoPoints;
