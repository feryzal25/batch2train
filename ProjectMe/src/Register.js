import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Alert,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import axios from 'axios';
import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import {SignUp} from './redux/Action/Action';
import backgroundImage from '../src/image/gambar/bgregis.jpeg';

const RegisterScreen = () => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const dispatch = useDispatch();

  const validateEmail = email => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  const validatePassword = password => {
    return password.length >= 8;
  };

  const handleRegister = () => {
    if (!username || !email || !password || !confirmPassword) {
      // Show error message
      Alert.alert('Error', 'Please fill in all fields');
      return;
    }
    if (password !== confirmPassword) {
      // Show error message
      Alert.alert('Error', 'Passwords do not match');
      return;
    }
    if (!validateEmail(email)) {
      // Show error message
      Alert.alert('Error', 'Please enter a valid email address');
      return;
    }
    if (!validatePassword(password)) {
      // Show error message
      Alert.alert(
        'Error',
        'Password must be at least 8 characters long and contain at least one uppercase letter, one lowercase letter, and one number',
      );
      return;
    }

    dispatch(SignUp(username, email, password, confirmPassword));

    // Send registration request to server
    axios
      .post(
        'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users.json',
        {
          username,
          email,
          password,
        },
      )
      .then(response => {
        if (response.data.success) {
        } else {
          Alert.alert('Registrasi Sukses!', response.data.message);
        }
      })
      .catch(error => {
        // Show error message
        Alert.alert('Error', 'An error occurred while registering');
      });
  };

  return (
    <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
      <View style={styles.container}>
        <Text style={styles.title}>REGISTRASI</Text>
        <View style={styles.inputContainer}>
          <Text style={styles.label}>Username</Text>
          <TextInput
            placeholder="Masukkan Username"
            style={styles.input}
            value={username}
            onChangeText={text => setUsername(text)}
          />

          <Text style={styles.label}>Email</Text>
          <TextInput
            placeholder="Masukkan Email Anda"
            style={styles.input}
            value={email}
            onChangeText={text => setEmail(text)}
            keyboardType="email-address"
          />

          <Text style={styles.label}>Password</Text>
          <TextInput
            placeholder="Atur Password "
            style={styles.input}
            value={password}
            onChangeText={text => setPassword(text)}
            secureTextEntry
          />

          <Text style={styles.label}>Confirm Password</Text>
          <TextInput
            placeholder="Input Kembali Password"
            style={styles.input}
            value={confirmPassword}
            onChangeText={text => setConfirmPassword(text)}
            secureTextEntry
          />
          <View style={styles.button}>
            <TouchableOpacity
              onPress={() => {
                handleRegister(username, email, password, confirmPassword);
              }}>
              <Text
                style={{
                  color: 'white',
                  fontWeight: 'bold',
                  alignItems: 'center',
                  justifyContent: 'center',
                  textAlign: 'center',
                }}>
                REGISTER
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 1,
  },
  button: {
    backgroundColor: '#068f8f',
    borderRadius: 5,
    marginTop: 8,
    paddingVertical: 10,
    paddingHorizontal: 10,
    margin: 2,
  },
  title: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  inputContainer: {
    width: 250,
  },
  label: {
    color: 'white',
    fontSize: 16,
    marginBottom: 5,
    fontWeight: 'bold',
  },
  input: {
    backgroundColor: '#fff',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#ccc',
    padding: 10,
    marginBottom: 10,
  },
  backgroundImage: {
    flex: 1,
    color: '#d6e5f2',
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default RegisterScreen;
