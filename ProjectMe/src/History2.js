import {
  View,
  Text,
  BackHandler,
  StyleSheet,
  ScrollView,
  Image,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Search from './Search';
import AscDsc from './AscDsc';
import {useDispatch, useSelector} from 'react-redux';
import {History} from './redux/Action/Action';

const Historys = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  const [ketikan, setKetikan] = useState('');
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [screens, setScreens] = useState(1);

  const dataASCDSC = async data => {
    const dataArray = Object.keys(data).map(key => ({
      sender: data[key].sender,
      target: data[key].target,
      amount: data[key].amount,
      type: data[key].type,
    }));
    setData(dataArray);
  };

  const dispatch = useDispatch();
  const historyy = () => {
    try {
      setLoading(true);
      dispatch(History());
      setTimeout(() => {
        setLoading(false);
      }, 2000);
    } catch (e) {
      console.log('error', e);
    }
  };

  const states = useSelector(state => state.Fetch?.History);
  const result = Object.entries(states);

  useEffect(() => {
    historyy();
    dataASCDSC(states);
  }, []);

  const sortAscending = () => {
    const sortedData = [...data].sort((a, b) => a.amount - b.amount);
    setData(sortedData);
  };

  const sortDescending = () => {
    const sortedData = [...data].sort((a, b) => b.amount - a.amount);
    setData(sortedData);
  };

  if (loading === true) {
    return <Text>Loading...</Text>;
  }
  return (
    <View style={styles.container}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <Search
          KetikanUser={ketikan}
          navigation={navigation}
          UbahKetikanUser={e => {
            setKetikan(e);
            setScreens(1);
          }}
        />
        <AscDsc
          onPress2={() => {
            sortAscending();
            setScreens(2);
          }}
          onPress1={() => {
            sortDescending();
            setScreens(2);
          }}
        />
      </View>
      <View>
        {screens == 1 ? (
          <ScrollView>
            {result?.map((e, index) => {
              const filter = e.filter(item => {
                return (
                  item?.type?.toLowerCase().includes(ketikan?.toLowerCase()) ||
                  item?.amount
                    ?.toString()
                    .toLowerCase()
                    .includes(ketikan?.toLowerCase()) ||
                  item?.sender
                    ?.toString()
                    .toLowerCase()
                    .includes(ketikan?.toLowerCase()) ||
                  item?.target
                    ?.toString()
                    .toLowerCase()
                    .includes(ketikan?.toLowerCase())
                );
              });

              return (
                <View style={styles.body}>
                  {filter.map((es, index) => {
                    return (
                      // kotak
                      <View style={styles.list} key={index}>
                        <View
                          style={{
                            flexDirection: 'column',
                          }}>
                          {/* judul&ket */}
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                            }}>
                            <Text style={styles.tipe}>{es.type}</Text>
                            <Image
                              source={{
                                uri: 'https://img.icons8.com/external-anggara-flat-anggara-putra/32/null/external-checklist-basic-user-interface-anggara-flat-anggara-putra.png',
                              }}
                              style={{height: 20, width: 20}}></Image>
                          </View>

                          {/* ROW */}
                          <View
                            style={{
                              flexDirection: 'row',
                            }}>
                            {/* data */}
                            <View
                              style={{
                                width: '85%',
                              }}>
                              <Text style={styles.data}>
                                Pengirim : {es.sender}
                              </Text>
                              <Text style={styles.data}>
                                Penerima : {es.target}
                              </Text>
                              <Text style={styles.data}>
                                Jumlah : {es.amount}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>
                    );
                  })}
                </View>
              );
            })}
          </ScrollView>
        ) : (
          <ScrollView>
            {data.length > 0 ? (
              data.map((es, index) => (
                <View style={styles.list} key={index}>
                  <View
                    style={{
                      flexDirection: 'column',
                    }}>
                    {/* judul&ket */}
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text style={styles.tipe}>{es.type}</Text>
                      <Image
                        source={{
                          uri: 'https://img.icons8.com/external-anggara-flat-anggara-putra/32/null/external-checklist-basic-user-interface-anggara-flat-anggara-putra.png',
                        }}
                        style={{height: 20, width: 20}}></Image>
                    </View>

                    {/* ROW */}
                    <View
                      style={{
                        flexDirection: 'row',
                      }}>
                      {/* gambar */}

                      {/* data */}
                      <View
                        style={{
                          width: '85%',
                        }}>
                        <Text style={styles.data}>Pengirim : {es.sender}</Text>
                        <Text style={styles.data}>Penerima : {es.target}</Text>
                        <Text style={styles.data}>Jumlah : {es.amount}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              ))
            ) : (
              <Text></Text>
            )}
          </ScrollView>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    justifyContent: 'center',
    alignContent: 'center',
    padding: 20,
    elevation: 1.5,
    flexDirection: 'row',
    borderRadius: 10,
    backgroundColor: '#1fccc6',
    marginTop: 8,
  },
  body: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#000',
  },
  data: {
    color: 'black',
    fontFamily: 'Montserrat-Regular',
    marginLeft: 10,
  },
  tipe: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
    marginLeft: 10,
    width: 150,
  },
  header: {
    padding: 10,
    backgroundColor: 'white',
    elevation: 5,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    marginBottom: 3,
  },
  back: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: 5,
  },
  text: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'black',
  },
});

export default Historys;
