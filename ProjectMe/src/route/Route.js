import * as React from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer, StackActions} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Uigojek from '../Uigojek';
import RadioButton from '../RadioBtn';
import Goride from '../Goride';
import Gocar from '../Gocar';
import GoFood from '../Gofood';
import GoBlueBird from '../Goblueride';
import GoSend from '../Gosend';
import GoPoints from '../Gopoints';
import GoPulsa from '../Gopulsa';
import Lainnya from '../Lainnya';
import History from '../History';
import Historys from '../History2';
import Headerss from '../Headerss';
import Transaksi from '../Transaksi';
import TextInputLabel from '../TextInputLabel';
import {ScreenStack} from 'react-native-screens';
import Bayar from '../Bayar';
import Search from '../Searchhh';
import RegisterScreen from '../Register';
import LoginScreen from '../Login';

const Stack = createNativeStackNavigator();

function Route() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        {/* <Stack.Screen name="Uigojek" component={Uigojek} />
      <Stack.Screen name="Goride" component={Goride} />
      <Stack.Screen name="Gocar" component={Gocar} />
      <Stack.Screen name="GoFood" component={GoFood} />
      <Stack.Screen name="GoBlueBird" component={GoBlueBird} />
      <Stack.Screen name="GoSend" component={GoSend} />
      <Stack.Screen name="GoPulsa" component={GoPulsa} />
      <Stack.Screen name="GoPoints" component={GoPoints} />
      <Stack.Screen name="Lainnya" component={Lainnya} />
      <Stack.Screen name="Headerss" component={Headerss} />
      <Stack.Screen name="History" component={History} />
      <Stack.Screen name="Historys" component={Historys} />
      <Stack.Screen name="Transaksi" component={Transaksi} />
      <Stack.Screen name="TextInputLabel" component={TextInputLabel} />
      <Stack.Screen name="Bayar" component={Bayar} />
      <Stack.Screen name="Search" component={Search} /> */}
        <Stack.Screen name="RadioButton" component={RadioButton} />
        {/* <Stack.Screen name="Login" component={LoginScreen} /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Route;
