import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import icPoint from './image/gambar/points.png';
import icPulsa from './image/gambar/pulsa.png';
import icSend from './image/gambar/send.png';
import icBlueb from './image/gambar/blueb.png';
import icFood from './image/gambar/food.png';
import icCar from './image/gambar/car.png';
import icRide from './image/gambar/ride.png';
import icLain from './image/gambar/lain.png';
import icBayar from './image/gambar/bayar.png';
import icPay from './image/gambar/paylater.png';
import icTambah from './image/gambar/tambah.png';
import icLainnya from './image/gambar/lainnya.png';
import icLogo from './image/gambar/logo.png';
import icPromo from './image/gambar/promo.png';
import History from './image/gambar/History.png';

const Home = ({navigation}) => {
  const [Change, setChange] = useState(0);
  const ChangeColor = itemID => {
    setChange(currentItem => (currentItem === itemID ? itemID : itemID));
  };
  const datass = [
    {label: 'Konten Buat Kamu', id: 0},
    {label: 'Apa Aja', id: 1},
    {label: 'Hiburan', id: 2},
    {label: 'Makanan', id: 3},
    {label: 'Gaya Hidup', id: 4},
  ];
  return (
    <View style={styles.container}>
      {/* SEARCH */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View style={styles.top1}>
          <View>
            <Image
              source={{
                uri: 'https://img.icons8.com/ios-filled/512/search--v1.png',
              }}
              style={{height: 23, width: 35, resizeMode: 'contain'}}
            />
          </View>
          <Text style={{textAlign: 'left', color: 'black'}}>
            Mau cari apa di Gojek?
          </Text>
        </View>

        <View style={styles.top2}>
          <View>
            <Image
              source={icPromo}
              style={{height: 23, width: 35, resizeMode: 'contain'}}
            />
          </View>
          <Text style={{textAlign: 'left', color: '#333333'}}>Promo</Text>
        </View>
      </View>

      <View>
        {/* kotak biru atas */}
        <View
          style={{
            flexDirection: 'row',
            backgroundColor: '#1059af',
            borderWidth: 0,
            height: 45,
            alignItems: 'center',
            borderTopRightRadius: 5,
            borderTopLeftRadius: 5,
            marginBottom: -1,
            justifyContent: 'flex-start',
            borderBottomColor: 'white',
            borderBottomWidth: 3,
          }}>
          <View>
            <Image
              source={icLogo}
              style={{
                height: 28,
                width: 30,
                marginLeft: 5,
                resizeMode: 'contain',
              }}
            />
          </View>
          <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20}}>
            gopay
          </Text>
        </View>
        {/* KOTAK BIRU */}
        <View
          style={{
            flexDirection: 'row',
            backgroundColor: '#1564c3',
            borderWidth: 0,
            height: 100,
            alignItems: 'center',
            borderBottomRightRadius: 5,
            borderBottomLeftRadius: 5,
            justifyContent: 'space-evenly',
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Bayar');
            }}>
            <View style={{alignItems: 'center', width: 80}}>
              <View style={styles.middle}>
                <Image
                  source={icBayar}
                  style={{height: 45, width: 150, resizeMode: 'contain'}}
                />
              </View>
              <Text style={{color: 'white', fontWeight: 'bold'}}>Bayar</Text>
            </View>
          </TouchableOpacity>

          <View style={{alignItems: 'center', width: 80}}>
            <View style={styles.middle}>
              <Image
                source={icPay}
                style={{height: 35, width: 100, resizeMode: 'contain'}}
              />
            </View>
            <Text style={{color: 'white', fontWeight: 'bold'}}>PayLater</Text>
          </View>

          <View style={{alignItems: 'center', width: 80}}>
            <View style={styles.middle}>
              <Image
                source={icTambah}
                style={{height: 45, width: 100, resizeMode: 'contain'}}
              />
            </View>
            <Text style={{color: 'white', fontWeight: 'bold'}}>Isi Saldo</Text>
          </View>

          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Historys');
            }}>
            <View style={{alignItems: 'center', width: 80}}>
              <View style={styles.middle}>
                <Image
                  source={History}
                  style={{height: 45, width: 100, resizeMode: 'contain'}}
                />
              </View>
              <Text style={{color: 'white', fontWeight: 'bold'}}>History</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>

      {/* MENU ATAS*/}
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingVertical: 10,
          marginTop: 5,
          justifyContent: 'space-evenly',
        }}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Goride');
          }}>
          <View style={{alignItems: 'center', width: 100}}>
            <View style={styles.ride}>
              <Image
                source={icRide}
                style={{height: 50, width: 60, resizeMode: 'center'}}
              />
            </View>
            <Text>GoRide</Text>
          </View>
        </TouchableOpacity>

        {/* gocar */}
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Gocar');
          }}>
          <View style={{alignItems: 'center', width: 100}}>
            <View style={styles.car}>
              <Image
                source={icCar}
                style={{height: 60, width: 150, resizeMode: 'contain'}}
              />
            </View>
            <Text>GoCar</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            navigation.navigate('GoFood');
          }}>
          <View style={{alignItems: 'center', width: 100}}>
            <View style={styles.food}>
              <Image
                source={icFood}
                style={{height: 50, width: 70, resizeMode: 'center'}}
              />
            </View>
            <Text>GoFood</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            navigation.navigate('GoBlueBird');
          }}>
          <View style={{alignItems: 'center', width: 100}}>
            <View style={styles.blueb}>
              <Image
                source={icBlueb}
                style={{height: 60, width: 150, resizeMode: 'contain'}}
              />
            </View>
            <Text>GoBlueBird</Text>
          </View>
        </TouchableOpacity>
      </View>

      {/* MENU BAWAH */}
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          elevation: 10,
          paddingVertical: 5,
          justifyContent: 'space-evenly',
        }}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('GoSend');
          }}>
          <View style={{alignItems: 'center', width: 105}}>
            <View style={styles.send}>
              <Image
                source={icSend}
                style={{height: 47, width: 100, resizeMode: 'contain'}}
              />
            </View>
            <Text>GoSend</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            navigation.navigate('GoPulsa');
          }}>
          <View style={{alignItems: 'center', width: 100}}>
            <View style={styles.pulsa}>
              <Image
                source={icPulsa}
                style={{height: 47, width: 100, resizeMode: 'contain'}}
              />
            </View>
            <Text>GoPulsa</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            navigation.navigate('GoPoints');
          }}>
          <View style={{alignItems: 'center', width: 100}}>
            <View style={styles.points}>
              <Image
                source={icPoint}
                style={{height: 47, width: 60, resizeMode: 'contain'}}
              />
            </View>
            <Text>GoPoints</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Lainnya');
          }}>
          <View style={{alignItems: 'center', width: 100}}>
            <View style={styles.lainnya}>
              <Image
                source={icLain}
                style={{height: 30, width: 30, resizeMode: 'contain'}}
              />
            </View>
            <Text>Lainnya</Text>
          </View>
        </TouchableOpacity>
      </View>

      {/* KONTEN */}
      <View>
        <Text
          style={{
            fontWeight: 'bold',
            color: 'black',
            fontSize: 20,
          }}>
          Konten Buat Kamu
        </Text>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {datass.map((item, index) => {
            const colors = Change == item.id ? '#fff' : '#000';
            return (
              <TouchableOpacity
                key={index}
                onPress={() => {
                  ChangeColor(item.id);
                }}>
                <View style={styles.conData(Change, item.id)}>
                  <Text
                    style={{
                      color: colors,
                    }}>
                    {item.label}
                  </Text>
                </View>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
        <View>
          <Image
            source={{
              uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSG1L0o2XTIf6Pc7oFX9pYKPmhw7BPzOOFUtlSuygEc&s',
            }}
            style={{
              height: 70,
              width: 90,
              resizeMode: 'contain',
              marginTop: -8,
            }}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
    padding: 20,
    margin: 5,
  },
  top1: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 20,
    borderColor: '#d9d9d9',
    height: 45,
    width: '67%',
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  top2: {
    backgroundColor: '#f7f7f8',
    borderWidth: 1,
    borderRadius: 20,
    borderColor: '#d9d9d9',
    height: 45,
    width: '26%',
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  middle: {
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ride: {
    height: 50,
    width: 50,
    borderRadius: 100,
    backgroundColor: '#00B200',
    borderWidth: 2,
    borderColor: '#00B200',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    overflow: 'hidden',
  },
  car: {
    height: 50,
    width: 50,
    borderRadius: 100,
    backgroundColor: '#00B200',
    borderWidth: 2,
    borderColor: '#00B200',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    overflow: 'hidden',
  },
  food: {
    height: 50,
    width: 50,
    borderRadius: 100,
    backgroundColor: '#ff0029',
    borderWidth: 2,
    borderColor: '#ff0029',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    overflow: 'hidden',
  },
  blueb: {
    height: 50,
    width: 50,
    borderRadius: 100,
    backgroundColor: '#00b2d6',
    borderWidth: 2,
    borderColor: '#00b2d6',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    overflow: 'hidden',
  },
  send: {
    height: 50,
    width: 50,
    borderRadius: 100,
    backgroundColor: '#00B200',
    borderWidth: 2,
    borderColor: '#00B200',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    overflow: 'hidden',
  },
  pulsa: {
    height: 50,
    width: 50,
    borderRadius: 100,
    backgroundColor: '#00b2d6',
    borderWidth: 2,
    borderColor: '#00b2d6',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    overflow: 'hidden',
  },
  points: {
    height: 50,
    width: 50,
    borderRadius: 100,
    backgroundColor: '#00b2d6',
    borderWidth: 2,
    borderColor: '#00b2d6',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    overflow: 'hidden',
  },
  lainnya: {
    height: 50,
    width: 50,
    borderRadius: 100,
    backgroundColor: 'white',
    borderWidth: 2,
    borderColor: '#d9d9d9',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    overflow: 'hidden',
  },
  conData: (click, data) => ({
    backgroundColor: click !== data ? '#fff' : '#00B200',
    borderRadius: 24,
    height: 34,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 15,
    paddingVertical: 5,
    marginRight: 4,
    borderWidth: 2,
    marginTop: 3,
    borderColor: click !== data ? '#d9d9d9' : '#00B200',
  }),
});

export default Home;
