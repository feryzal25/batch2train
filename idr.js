//deklrasi function dan pengkondisian
const currencyFormater = (value) => {
  if (value == "") {
    return "IDR 0,00";
  }

  //deklrasi variabel
  let valueFormatted = "";
  let j = 0;

  //pengulangan ternary operator
  for (let i = value.length - 1; i >= 0; i--) {
    j++;
    valueFormatted = value[i] + valueFormatted;
    if (j % 3 === 0) {
      valueFormatted = "." + valueFormatted;
    }
    console.log(valueFormatted);
  }

  //kondisi pengambilan data string
  if (valueFormatted[0] === ".") {
    valueFormatted = valueFormatted.substring(1, valueFormatted.length);
  }

  valueFormatted = "IDR " + valueFormatted + ",00";
  return valueFormatted;
};

console.log(currencyFormater("1000"));
