function pickerText(arr, i) {
  if (i < 0 || i >= arr.length) {
    return "throw error: Invalid weekday number";
  } else {
    const ChoseText = arr[i];
    return ChoseText;
  }
}

const weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];
console.log(pickerText(weekdays, 0));
console.log(pickerText(weekdays, 3));
console.log(pickerText(weekdays, 5));
